#include "stdafx.h"
#include "flowerpatch.h"
#include "swarmmaker.h"

FlowerPatch::FlowerPatch(void)
{
	scout=NULL;
	stagnation=0;
}

FlowerPatch::~FlowerPatch(void)
{
	scout=NULL;
}

/* ---------------------------------------------------- */
/* this function duplicates an instance of the class.	*/
/* input: none.											*/
/* return: the duplicated instance.						*/
/* ---------------------------------------------------- */
FlowerPatch* FlowerPatch::duplicate()
{
	FlowerPatch* copy=new FlowerPatch();

	copy->setImprovement(getImprovement());
	copy->setScout(getScout()->duplicate());
	copy->setSwarmSize(getSwarmSize());
	copy->setWidth(getWidth());
	copy->setStagnation(getStagnation());

	return copy;
}

void FlowerPatch::setImprovement(bool fitnessHasImproved)
{
	improvement=fitnessHasImproved;
}

bool FlowerPatch::getImprovement()
{
	return improvement;
}

void FlowerPatch::setSwarmSize(int bees)
{
	swarmSize=bees;
}

int FlowerPatch::getSwarmSize()
{
	return swarmSize;
}

void FlowerPatch::setWidth(float neighbourhoodSize)
{
	getScout()->setParametersMutationRange(neighbourhoodSize);
}

float FlowerPatch::getWidth()
{
	return getScout()->getParametersMutationRange();
}

void FlowerPatch::setScout(Solution* bee)
{
	scout=bee;
}

Solution* FlowerPatch::getScout()
{
	return scout;
}

void FlowerPatch::updateStagnation()
{
	if(getImprovement()==true)	// if there is improvement, the stagnation counter is reset
	{
		stagnation=0;
	}
	else
	{
		stagnation++;
	}
}

void FlowerPatch::setStagnation(int cycles)
{
	stagnation=cycles;
}

int FlowerPatch::getStagnation()
{
	return stagnation;
}
