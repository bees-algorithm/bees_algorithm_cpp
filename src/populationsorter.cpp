#include "stdafx.h"
#include "populationsorter.h"

PopulationSorter::PopulationSorter(void)
{
}

PopulationSorter::~PopulationSorter(void)
{
}

void PopulationSorter::sort(int popSize, Solution** population)
{
	int i;
	int j;
	int inc=1;
	float pivotFitness;
	float tempFitness;
	Solution* pivot;
	Solution* temp;

	do 
	{
		inc*=3;
		inc++;
	} while(inc<=popSize);

	do 
	{
		inc/=3;
		for(i=inc;i<popSize;i++)
		{
			// pivot element
			pivot=population[i];
			pivotFitness=pivot->getFitness();
			j=i;
			// comparison element
			temp=population[j-inc];
			tempFitness=temp->getFitness();
			while(tempFitness>pivotFitness)
			{
				population[j]=population[j-inc];
				j-=inc;
				if(j<inc)
					break;
				temp=population[j-inc];
				tempFitness=temp->getFitness();
			}
			population[j]=pivot;
		}
	} while(inc>1);
}

void PopulationSorter::sort(int popSize, FlowerPatch** population)
{
	int i;
	int j;
	int inc=1;
	float pivotFitness;
	float tempFitness;
	FlowerPatch* pivot;
	FlowerPatch* temp;

	do 
	{
		inc*=3;
		inc++;
	} while(inc<=popSize);

	do 
	{
		inc/=3;
		for(i=inc;i<popSize;i++)
		{
			// pivot element
			pivot=population[i];
			pivotFitness=pivot->getScout()->getFitness();
			j=i;
			// comparison element
			temp=population[j-inc];
			tempFitness=temp->getScout()->getFitness();
			while(tempFitness>pivotFitness)
			{
				population[j]=population[j-inc];
				j-=inc;
				if(j<inc)
					break;
				temp=population[j-inc];
				tempFitness=temp->getScout()->getFitness();
			}
			population[j]=pivot;
		}
	} while(inc>1);
}
