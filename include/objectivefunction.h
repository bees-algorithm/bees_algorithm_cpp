/* ---------------------------------------------------- */
/* this class stores the benchmark minimisation 		*/ 
/* functions and their parameters.						*/
/* ---------------------------------------------------- */

#pragma once
#include "stdafx.h"

class ObjectiveFunction
{
private:
	benchmarkFunction function;				/* type of benchmark function (see definitions.h) */
	int inputs;								/* number of input variables (i.e. number of optimisation parameters) */
	float shekelVector(int i);
	float langermannVector(int i);
	float shekelMatrix(int i, int j);
public:
	ObjectiveFunction(void);
	~ObjectiveFunction(void);
	ObjectiveFunction* duplicate();
	void setFunction(benchmarkFunction benchmark);
	benchmarkFunction getFunction();
	void setInputs(int nr);
	int getInputs();
	float* getInputPattern();
	float getOutput(float* inputPattern);
	float getOptimum();
	float getVariablesRange();
};
