/* ---------------------------------------------------- */
/* this class stores the learning parameters of the		*/
/* Bees Algorithm. These parameters are defined by		*/
/* the user in file beesettings.par.					*/
/* ---------------------------------------------------- */

#pragma once

class BeeSettings{
private:
	int scoutBees;						/* scout bees (n) */
	int eliteSites;						/* elite bees (e) */
	int bestSites;						/* best bees (m) */
	int recruitedEliteSites;			/* recruited bees for elite sites (ne) */
	int recruitedBestSites;				/* recruited bees for best sites (nb) */
	int stagnationLimit;				/* iterations limit for site abandonment */
	float neighbourhoodSize;			/* initial size of neighbourhood (ngh) */
	char* beeSettingsFile;				/* file where learning parameters are stored */
public:
	BeeSettings(char* settingsFileName);
	BeeSettings(int n,	int e, int m,	int ne,	int nb, int cycles,	float ngh);
	~BeeSettings(void);
	void readSettings();
	void copySettings(BeeSettings* origin);
	void setScoutBees(int bees);
	int getScoutBees();
	void setEliteSites(int sites);
	int getEliteSites();
	void setBestSites(int sites);
	int getBestSites();
	void setRecruitedEliteSites(int bees);
	int getRecruitedEliteSites();
	void setRecruitedBestSites(int bees);
	int getRecruitedBestSites();
	void setStagnationLimit(int cycles);
	int getStagnationLimit();
	void setNeighbourhoodSize(float delta);
	float getNeighbourhoodSize();
	void setBeeSettingsFile(char* fileName);
	char* getBeeSettingsFile();
};
