/* ---------------------------------------------------- */
/* definitions of enum types							*/ 
/* ---------------------------------------------------- */

#pragma once

enum action {r, w};																						
enum benchmarkFunction {ackley, easom, goldstein_price, griewank, hypersphere, langermann, martin_gaddy, michalewicz, rastrigin, rosenbrock, schwefel, schaffer, shekel, steps};
enum terminatingCondition {fixedCycles, fitness};
