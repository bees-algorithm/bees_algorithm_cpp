/* ---------------------------------------------------- */
/* this class contains methods to create new swarms	of  */
/* bees (i.e. solutions), single bees, and modify the 	*/
/* parameters of existing solutions. 					*/
/* ---------------------------------------------------- */
#pragma once
#include "solution.h"
#include "settings.h"

class SwarmMaker
{
private:
	bool useCustomFunction;
	unsigned int n_dimensions;
	float custom_range;
public:
	SwarmMaker(void);
	SwarmMaker(unsigned int,float);
	~SwarmMaker(void);
	Solution** createRandomSwarm(int swarmSize, Settings* parameters);
	Solution* createSolution(Settings* parameters);
	void mutateParameters(Solution* mutant);
};
