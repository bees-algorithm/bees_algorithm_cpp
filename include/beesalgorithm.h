/* ---------------------------------------------------- */
/* this class defines the Bees Algorithm.				*/
/* ---------------------------------------------------- */
#pragma once
#include "stdafx.h"
#include "flowerpatch.h"
#include "populationsorter.h"
#include "swarmmaker.h"
using namespace std;

class BeesAlgorithm
{
protected:
	int restarted;						// number of sites abandoned (used for computation of total number of fitness evaluations).
	Solution** swarm;					// scout bees (array of pointers to solutions).
	FlowerPatch** patches;				// flower patches searched at a time (array of pointers to flower patches).
	Solution* bestBee;					// best-so-far solution.
	SwarmMaker* maker;					// creator and modifier of solutions.
	PopulationSorter* sorter;			// class containing the algorithms for sorting the population.
	Settings* parameters;				// experimental settings
	// used only in the linked library
	float (*custom_objective_function)(float*,unsigned int);
	unsigned int n_dimensions, max_iterations;
	float range, fitness_tollerance;
public:
	BeesAlgorithm(void);
	BeesAlgorithm(float (*f)(float*,unsigned int), unsigned int n_dim, float ran, int n,	int e, int m,	int ne,	int nb, int cycles, float ngh);
	~BeesAlgorithm(void);
	void checkAndSetFitness(Solution*);
	void initialisation();
	void deletePopulation();
	void evaluation(Solution** population, int popSize);
	void harvest(FlowerPatch* foodSource);
	void localSearch(int selectedSites);
	FlowerPatch* randomPatch();
	FlowerPatch** globalSearch(int randomScouts);
	void waggleDance();
	void foragingStep();
	void learn(int trialNr, ofstream &outFile);

	void setStoppingCriterion(terminatingCondition,float);
	bool stoppingCondition(Solution** population, int popSize, int gen);
	void updateBestBee();
	void updatePop(FlowerPatch** globalSearchScouts);
	void printBest(int iteration, Solution* best, ostream &outFile);

	void setRestarted(int count);
	void incrementRestarted();
	int getRestarted();
	void setSolution(int nr, Solution* bee);
	Solution* getSolution(int nr);
	void setSwarm(Solution** colony);
	Solution** getSwarm();
	void setPatch(int nr, FlowerPatch* patch);
	FlowerPatch* getPatch(int nr);
	FlowerPatch** getAllPatches();
	void setBestBee(Solution* bee);
	Solution* getBestBee();
	void setMaker(SwarmMaker* creator);
	SwarmMaker* getMaker();
	void setSorter(PopulationSorter* routine);
	PopulationSorter* getSorter();
	void setParameters(Settings* settings);
	Settings* getParameters();
};
