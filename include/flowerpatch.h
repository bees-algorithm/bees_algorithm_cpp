/* ---------------------------------------------------- */
/* this class defines a flower patch. Every flower 		*/
/* patch is associated to a solution (scout bee) which	*/
/* defines its location and fitness						*/
/* ---------------------------------------------------- */
#pragma once
#include "settings.h"
#include "solution.h"

class FlowerPatch
{
private:
	bool improvement;		// flag that signals whether there was fitness improvement within the patch in the last local search cycle.
	int swarmSize;			// number of forager bees allocated to the patch via the waggle dance (respectively ne and nb for elite and best patches).
	int stagnation;			// consecutive cycles of stagnation
	Solution* scout;		// scout bee associated to the patch
public:
	FlowerPatch(void);
	~FlowerPatch(void);
	FlowerPatch* duplicate();
	void setImprovement(bool fitnessHasImproved);
	bool getImprovement();
	void setSwarmSize(int bees);
	int getSwarmSize();
	void setWidth(float neighbourhoodSize);
	float getWidth();
	void setScout(Solution* bee);
	Solution* getScout();
	void setStagnation(int cycles);
	int getStagnation();
	void updateStagnation();
};
