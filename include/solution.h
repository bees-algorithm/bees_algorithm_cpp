/* ---------------------------------------------------- */
/* this class stores the parameters and methods			*/
/* defining a numerical solution.						*/
/* ---------------------------------------------------- */
#pragma once
#include "definitions.h"
#include "objectivefunction.h"

class Solution
{
protected:
	float range;						// half-width W of solution space. For every solution x: -W<=x<=W. All parameters are defined over the same interval [-W,+W].
	float* parameters;					// vector of parameters that define a candidate solution.
	ObjectiveFunction* function;		// this is the "environment" (i.e. fitness function) where a solution evolves.
	float parametersMutationRange;		// maximum width for the mutation of the parameters characterising an individual (actually within [0.0001,1], see function setParametersMutationRange(float value))
	float fitness;						// fitness of individual
public:
	Solution();
	~Solution(void);
	Solution* duplicate();
	void setFitness(float value);
	float getFitness();
	void initialiseParameters(int nrOfParameters);
	void setParameter(int nr, float value);
	float getParameter(int nr);
	inline float* getParameters(){return parameters;}
	void incrementParameter(int nr, float delta);
	void setParametersMutationRange(float value);
	float getParametersMutationRange();
	void setFunction(ObjectiveFunction* func);
	ObjectiveFunction* getFunction();
	float getFunctionOutput();
	void setRange(float span);
	float getRange();
};
