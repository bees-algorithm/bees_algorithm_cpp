/* ---------------------------------------------------- */
/* this class contains the methods to sort instances of */ 
/* Solution and FlowerPatch classes in ascending order  */
/* of objective function evaluation. Since minimisation */
/* problems are considered, the methods effectively		*/
/* order the elements in descending order of fitness. 	*/
/* The sorting routine is 'QuickSort' from Numerical	*/
/* Recipes in C++ (if I remember correctly)				*/
/* ---------------------------------------------------- */
#pragma once
#include "flowerpatch.h"
#include "solution.h"

class PopulationSorter
{
public:
	PopulationSorter(void);
	virtual ~PopulationSorter(void);
	void sort(int popSize, Solution** population);
	void sort(int popSize, FlowerPatch** population);
};
