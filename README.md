# Bees Algorithm

This is the main repository of the Bees Algorithm and contains an implementation in C++.

The aim is to make available to everyone a library, built with the minimal number of dependencies, which can be easily integrated in larger projects, as well as used out-of-the-box to solve specific problems.
The code here presented is a modified version of the standard Bees Algorithm implementation that can be found on the [official Bees Algorithm website](http://beesalgorithmsite.altervista.org).

The Bees Algorithm is an intelligent optimization technique belonging to the swarm algorithms field.
Given a parametric objective function, the goal of the algorithm is to find the parameter values that maximise/minimise the output of the objective function.

Many real-world problems can be modeled as the optimisation of a parametric objective function, therefore effective algorithms to handle this kind of problems are of primary importance in many fields.
The Bees Algorithm performs simultaneus aggressive local searches around the most promising parameter settings of the objective function.
The algorithm is proven to outperform other intelligent optimisation techniques in many benchmark functions<sup>[2][3][4]</sup> as well as real world problems.

On top of this C++ version, implmentations of the bees Algorithm in [Python](https://gitlab.com/bees-algorithm/bees_algorithm_python) and [Matlab](https://gitlab.com/bees-algorithm/bees_algorithm_matlab) are also available in the respective repositories. 

# Introduction on the Bees Algorithm
<img src="https://images.pexels.com/photos/1043059/pexels-photo-1043059.jpeg"
     align="left"
     width="333" height="250" />

The Bees Algorithm is a nature-inspired search method that mimics the foraging behaviour of honey bees. It was created by [Prof. D.T. Pham](https://www.birmingham.ac.uk/staff/profiles/mechanical/pham-duc.aspx) and his co-workers in 2005<sup>[1]</sup>, and described in its standard formulation by Pham and Castellani<sup>[2]</sup>.

The algorithm uses a population of agents (artificial bees) to sample the solution space. A fraction of the population (scout bees) searches randomly for regions of high fitness (global search). The most successful scouts recruit a variable number of idle agents (forager bees) to search in the proximity of the fittest solutions (local search). Cycles of global and local search are repeated until an acceptable solution is discovered, or a given number of iterations have elapsed.

The standard version of the Bees Algorithm includes two heuristics: *neighbourhood shrinking* and *site abandonment*.
Using neighbourhood shrinking the size of the local search is progressively reduced when local search stops progressing on a given site.
The site abandonment procedure interrupts the search at one site after a given number of consecutive stagnation cycles, and restarts the local search at a randomly picked site.


<img src="pics/algorithm.png"
     align="right"
     width="300" height="300" />

The algorithm requires a number of parameters to be set, namely: the number of scout bees (*ns*), number of sites selected out of *ns* visited sites (*nb*), number of elite sites out of *nb* selected sites (*ne*), number of bees recruited for the best *ne* sites (*nre*), number of bees recruited for the other (*nb-ne*) selected sites (*nrb*).
The heuristics also require the set of the initial size of the patches (*ngh*) and the number of cycles after which a site is abandoned (*stlim*).
Finally, the stopping criterion must be defined. 

The algorithm starts with the *ns* scout bees being placed randomly in the search space and the main algorithm steps can be summarised as follows:


1. Evaluate the fitness of the population according the objective function;
2. Select the best *nb* sites for neighbourhood (local) search;
3. Recruit *nrb* forager bees for the selected sites (*nre* bees for the best *ne* sites) and evaluate their fitnesses;
4. Select the fittest bee from each local site as the new site centre;
5. If a site fails to improve in a single local search, its neighbourhood size is reduced (neighbourhood shrinking);
6. If a site fails to improve for *stlim* cycles, the site is abandoned (site abandonment);
7. Assign the remaining bees to search uniformly the whole search space and evaluate their fitnesses;
8. If the stopping criterion is not met, return to step 2;

For more information please refer to the [official Bees Algorithm website](http://beesalgorithmsite.altervista.org) and the [wikipedia page](https://en.wikipedia.org/wiki/Bees_algorithm).

# Installation
This repository allows to build 3 different components:

- **bees_algorithm_legacy** the bees algorithm program exactly as is built from the source code present in the [official site](http://beesalgorithmsite.altervista.org/);
- **libbees_algorithm.so** a shared library that can be linked dinamically in your program;
- **bees_algorithm_tester** a tester program that use the shared library and, when run, it replicate exactly the behaviour of **bees_algorithm_legacy**;

To build the components the following commands must be used:
```sh
$ cmake .
$ make
$ sudo make install
```
After these commands are ran all the components should be found in the local directory.
The library **libbees_algorithm.so** and the header files (*.h) should also be installed in the system, therefore the library can be used from any path whithin the system.

# Library Usage
In order to use the Bees Algorithm, the relative library must be included
```cpp
#include <beesalgorithm.h>
```
Then an objective function must be defined (here's the [Ackley](https://www.sfu.ca/~ssurjano/ackley.html) function as example):
```cpp
float Ackley(float* point, unsigned int n_dims){
	float sum1, sum2;
	sum1=sum2=0;
	for(unsigned int i=0;i<n_dims;i++){
		sum1+=powf(point[i], 2);
		sum2+=cos(2*M_PI*point[i]);
	}
	sum1/=n_dims;
	sum2/=n_dims;
	return 20+exp(1.0F)-20*exp(-0.2F*sqrt(sum1))-exp(sum2);
}
```
The objective function can be anything, but it must respect the following singature:
```cpp
float f(float*, unsigned int)
```
which takes a point as *float* array, the size of the array (that is, the number of dimensions of the problem) and it returns the function's output as a float.
It's also important to note that this implementation of the Bees Algorithm *always* try to **maximise** the objective function.
Therefore, if you have a minimisation problem, be wary to return the opposite of the output value (as it is in this implementation of the Ackley function).

The algorithm can be instantiated using the **BeesAlgorithm** class with the following constructor:
```cpp
BeesAlgorithm(float (*f)(float*, unsigned int), unsigned int n_dim, float ran, int n, int e, int m, int ne, int nb, int cycles, float ngh);
```
where the parameters are as follows:

- **float (\*f)(float\*, unsigned int)** is the objective function;
- **n_dim** the number of dimensions of the problem;
- **ran** is the range where the search will be performed (in this implementation only symmetric boundaries centered in 0 can be used);
- **n** size of colony (*n=ne·nre+(nb-ne)·nrb+ns*);
- **e** elite sites (*ne*, $`ne\leq nb`$);
- **m** best sites (*nb*);
- **ne** recruited *nre* bees for elite sites;
- **nb** recruited *nrb* bees for best sites;
- **cycles** limit *stlim* of stagnation cycles for site abandonment;
- **ngh** initial size *a(0)* of neighbourhood. This is expressed as a fraction of a variable's range;

An example of initialisation of these parameters (suited for the Ackley function) is as follows:
```cpp
unsigned int n, e, m, ne, nb, cycles, n_dimensions;
float ngh, range;
n=100;
e=1;
m=8;
ne=20;
nb=10;
ngh=1.0F;
cycles=5;
n_dimensions=10;
range=32;
```
With this parameters, the algorithm can be instantiated as:
```cpp
BeesAlgorithm* bayBA = new BeesAlgorithm(Ackley,n_dimensions,range,n, e, m, ne, nb, cycles, ngh);
```
Afterwards, one or more stop criteria can be added:
```cpp
bayBA->setStoppingCriterion(fixedCycles,5000);
bayBA->setStoppingCriterion(fitness,0.001F);
```
Then the algorithm can be executed:
```cpp
ofstream outFile("test_out.txt");

unsigned int n_iterations=50;
for(unsigned int i=0;i<n_iterations;i++){
	bayBA->learn(i, outFile);
	outFile<< "\n";
}
outFile.close();
```
This will produce a result file *test_out.txt* in the current directory.

A complete working test for a standalone program that use the Bees Algorithm library can be found in **src/ba_tester.cpp** and it's compiled in the file **bees_algorithm_tester** in the current directory.

# Testing
To test the bees algorithm it's possible to run **bees_algorithm_legacy** from the current directory, as:
```sh
$ ./bees_algorithm_legacy
```
afterwards, you should be able to find the results of 50 runs of the bees algorithm in the file **results.txt**.

If you wish to try different Bees Algorithm parameters and/or using a different benchmark objective function, just modify the relative ".par" files in the **settings** directory.

# References

- [1]: Pham, Duc Truong, et al. "[The Bees Algorithm—A Novel Tool for Complex Optimisation Problems.](https://s3.amazonaws.com/academia.edu.documents/37269572/Pham06_-_The_Bee_Algorithm.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1550167392&Signature=%2BAoKD8QFeUIXvpEjojuAxWu2y0k%3D&response-content-disposition=inline%3B%20filename%3DThe_Bees_Algorithm_-_A_Novel_Tool_for_Co.pdf)" Intelligent production machines and systems. 2006. 454-459.
- [2]: Pham, Duc Truong, and Michele Castellani. "[The bees algorithm: modelling foraging behaviour to solve continuous optimization problems.](https://www.researchgate.net/profile/Marco_Castellani2/publication/229698041_The_Bees_Algorithm_Modelling_foraging_behaviour_to_solve_continuous_optimization_problems/links/0912f50107f349b7de000000/The-Bees-Algorithm-Modelling-foraging-behaviour-to-solve-continuous-optimization-problems.pdf)" Proceedings of the Institution of Mechanical Engineers, Part C: Journal of Mechanical Engineering Science 223.12 (2009): 2919-2938.
- [3]: Pham, Duc Truong, and Marco Castellani. "[Benchmarking and comparison of nature-inspired population-based continuous optimisation algorithms.](https://link.springer.com/article/10.1007/s00500-013-1104-9)" Soft Computing 18.5 (2014): 871-903.
- [4]: Pham, Duc Truong, and Marco Castellani. "[A comparative study of the Bees Algorithm as a tool for function optimisation.](https://www.tandfonline.com/doi/full/10.1080/23311916.2015.1091540)" Cogent Engineering 2.1 (2015): 1091540.

# Applications
The list below includes some applications of the Bees Algorithm. If you find the list incomplete and would like to see other applications added, please contact (m.castellani[at]bham.ac.uk).

 

## Optimisation of classifiers / clustering systems

- Pham D.T., Soroka A.J., Ghanbarzadeh A., Koç E., Otri S., Packianather M.S. (2006), Optimising Neural Networks for Identification of Wood Defects Using the Bees Algorithm, Proceedings IEEE International Conference on Industrial Informatics, Singapore, 1346-1351.
- Pham D.T., Darwish A.H. (2010), Using the bees algorithm with Kalman filtering to train an artificial neural network for pattern classification, Journal of Systems and Control Engineering 224(7), 885-892.
- Pham D.T., Zaidi M., Mahmuddin M., Ghanbarzadeh A., Koç E., Otri S. (2007), Using the bees algorithm to optimise a support vector machine for wood defect classification, IPROMS 2007 Innovative Production Machines and Systems Virtual Conference.
- Pham D.T., Suarez-Alvarez M.M., Prostov Y.I. (2011), Random search with k-prototypes algorithm for clustering mixed datasets, Proceedings Royal Society, 467, 2387-2403.

 

## Manufacturing

- Pham D.T., Soroka A.J., Koç E., Ghanbarzadeh A., Otri S. (2007), Some applications of the Bees Algorithm in engineering design and manufacture, Proceedings International Conference on Manufacturing Automation (ICMA 2007), Singapore.
- Pham D.T., Castellani M., Ghanbarzadeh A. (2007), Preliminary design using the Bees Algorithm, Proceedings Eigth LAMDAMAP International Conference on Laser Metrology, CMM and Machine Tool Performance. Cardiff - UK, 420-429.
- Tudu B., Majumder S., Mandal K.K., Chakraborty N. (2011), Optimal unit sizing of stand-alone renewable hybrid energy system using bees algorithm, Proceedings of 2011 International Conference on Energy, Automation, and Signal (ICEAS), Bhubaneswar, Odisha, India, IEEE Press, 1-6.
- Xu W., Zhou Z., Pham D.T., Liu Q., Ji C., Meng W. (2012), Quality of service in manufacturing networks: a service framework and its implementation, International Journal Advanced Manufacturing Technology, 63(9-12), 1227-1237.
- Moradi S., Razi P., Fatahi L. (2011), On the application of bees algorithm to the problem of crack detection of beam-type structures, Computers and Structures 89, 2169–2175.
- Pham, D.T., Otri S., Darwish A.H. (2007), Application of the Bees Algorithm to PCB assembly optimisation, Proceedings 3rd International Virtual Conference on Intelligent Production Machines and Systems (IPROMS 2007), Whittles, Dunbeath, Scotland, 511-516.
- Pham D.T., Koç E., Lee J.Y., Phrueksanant J. (2007), Using the Bees Algorithm to Schedule Jobs for a Machine, Proceedings 8th international Conference on Laser Metrology, CMM and Machine Tool Performance (LAMDAMAP). Cardiff, UK, Euspen, 430-439.
- Baykasoglu A., Özbakir L., Tapkan P. (2009), The bees algorithm for workload balancing in examination job assignment, European Journal Industrial Engineering 3(4) 424-435.
- Özbakir L., Tapkan P. (2011), Bee colony intelligence in zone constrained two-sided assembly line balancing problem, Expert Systems with Applications 38, 11947-11957.

 

## Control

- Pham D.T., Castellani M., Fahmy A.A. (2008), Learning the Inverse Kinematics of a Robot Manipulator using the Bees Algorithm, 2008 6th IEEE International Conference on Industrial Informatics (INDIN 2008), pp. 493-498.
- Pham D.T., Kalyoncu M. (2009), Optimisation of a fuzzy logic controller for a flexible single-link robot arm using the Bees Algorithm, Proceedings 7th IEEE International Conference on Industrial Informatics, 2009, INDIN 2009, Cardiff, UK, 475-480.
- Pham D.T., Darwish A.H., Eldukhri E.E. (2009), Optimisation of a fuzzy logic controller using the Bees Algorithm, International Journal of Computer Aided Engineering and Technology, 1, 250-264.
- Fahmy A.A., Kalyoncu M., Castellani M. (2012), Automatic Design of Control Systems for Robot Manipulators Using the Bees Algorithm, Proceedings of the Institution of Mechanical Engineers, Part I: Journal of Systems and Control Engineering, 226(4), 497-508.
- Alfi A., Khosravi A., Razavi S.E. (2011), Bee Algoritm–Based Nolinear Optimal Control Applied To A Continuous Stirred-Tank Chemical Reactor, Global Journal of Pure & Applied Science and Technology - GJPAST 1(2), 73-79.
- Castellani M., Pham Q.T., Pham D.T. (2012), Dynamic Optimisation by a Modified Bees Algorithm, Proceedings of the Institution of Mechanical Engineers, Part I: Journal of Systems and Control Engineering, vol. 226, no. 7, pp. 956–971.
- Pham Q.T., Pham D.T., Castellani M. (2012), A modified bees algorithm and a statistics-based method for tuning its parameters, Proceedings of the Institution of Mechanical Engineers, Part I: Journal of Systems and Control Engineering, 226, 287-301.


## Various optimisation problems

- Özbakir L., Baykasoglu A., Tapkan P. (2010), Bees algorithm for generalized assignment problem,” Applied Mathematics and Computation, 215, 3782–3795.
- Xu S., Yu F., Luo Z., Ji Z., Pham D.T., Qiu R. (2011), Adaptive Bees Algorithm - Bioinspiration from Honeybee Foraging to Optimize Fuel Economy of a Semi-Track Air-Cushion Vehicle, The Computer Journal 54(9), 1416-1426.
- Guney K., Onay M. (2010), Bees algorithm for interference suppression of linear antenna arrays, Expert Systems with Applications 37, 3129–3135.
- Guney K., Onay M. (2011), Synthesis of thinned linear antenna arrays using bees algorithm, Microwave and Optical Technology Letters 53, 795–799.
- Kavousi A., Vahidi B., Salehi R., Bakhshizadeh M.K., Farokhnia N., Fathi S.H. (2012), Application of the Bee Algorithm for Selective Harmonic Elimination Strategy in Multilevel Inverters, IEEE Transactions on Power Electronics 27(4) 1689-1696.
- Khang N.T.T.M., Phuc N.B., Nuong T.T.H. (2011), The Bees Algorithm for A Practical University Timetabling Problem in Vietnam, Proceedings of IEEE International Conference Computer Science and Automation Engineering (CSAE), Shanghai, China, 42-47.
- Nguyen K., Nguyen P., Tran N. (2012), A hybrid algorithm of Harmony Search and Bees Algorithm for a University Course Timetabling Problem, IJCSI International Journal of Computer Science Issues, 9(1), 12-17.
- Malik M. (2012), Bees algorithm for degree-constrained minimum spanning tree problem, National Conference Computing and Communication Systems (NCCCS), Durgapur, India, IEEE, 1-8.
- Derelia T., Das G.S. (2011), A hybrid ‘bee(s) algorithm’ for solving container loading problems, Applied Soft Computing 11, 2854–2862.
- Z. Khanmirzaei and M. Teshnehlab, “Prediction Using Recurrent Neural Network Based Fuzzy Inference system by the Modified Bees Algorithm,” International Journal of Advancements in Computing Technology, vol. 2, no. 2, pp. 42-55, 2010.Bioengineering
- Bahamish H.A.A., Abdullah R., Salam R.A. (2008), Protein Conformational Search Using Bees Algorithm, Second Asia International Conference on Modeling & Simulation (AICMS 08), Kuala Lumpur, Malaysia, IEEE Press, 911-916.
- Pham, D.T., Castellani, M., Le-Thi, H.A. (2013), The Bees Algorithm: Modelling Nature to Solve Complex Optimisation Problems, 11th International Conference on Manufacturing Research (ICMR2013), 19-20 September 2013, Cranfield, UK.Ruz G.A., Goles E. (2013), Learning gene regulatory networks using the bees algorithm, Neural Computing and Applications, 22(1) 63-70.
- Chai-ead, N., Aungkulanon P., Luangpaiboon P. (2011), Bees and Firefly Algorithms for Noisy Non-Linear Optimisation Problems, Proceedings of International Multi-Conference of Engineers and Computer Scientists 2011 (IMECS 2011). Hong Kong, China, Springer, 1449-1454.
- Pham D.T., Koç E. (2011), Design of a two-dimensional recursive filter using the bees algorithm, International Journal Automation and Computing 7(3) 399-402.

 

## Multi-objective optimisation

- Lee J.Y., Darwish A.H. (2008), Multi-objective Environmental/Economic Dispatch Using the Bees Algorithm with Weighted Sum, Proceedings of the EU-Korea Conference on Science and Technology (EKC2008), Ed. S.D. Yoo, Heidelberg, D, Springer Berlin Heidelberg, 267-274.
- Anantasate S., Bhasaputra P. (2011), A Multi-objective Bees Algorithm for Multi-objective Optimal Power Flow Problem, Proceedings of 8th International Conference on Electrical Engineering/Electronics, Computer, Telecommunications and Information Technology (ECTI-CON), Khon Kaen, Thailand, IEEE Press, 852-856.
- Sayadi F., Ismail M., Misran N., Jumari K. (2009), Multi-Objective Optimization Using the Bees Algorithm in Time-Varying Channel for MIMO MC-CDMA Systems, European Journal of Scientific Research 33(3), 411-428.
- Mansouri Poor M., Shisheh Saz M. (2012), Multi-Objective Optimization of Laminates with Straight Free Edges and Curved Free Edges by Using Bees Algorithm, American Journal of Advanced Scientific Research 1(4), 130-136.

 

## Other

- Saad E., Awadalla M., Darwish R. (2008), A Data Gathering Algorithm for a Mobile Sink in Large-Scale Sensor Networks, Proceedings Fourth International Conference on Wireless and Mobile Communications (ICWMC '08), Athens, Greece, 207-213.
- Tran Q.D., Liatsis P., Zhu B., He C. (2011), An Approach for Multimodal Biometric Fusion Under The Missing Data Scenario, Proceedings of 2011 International Conference on Uncertainty Reasoning and Knowledge Engineering. Bali, Indonesia, IEEE Presss, 185-188.
- Dhurandher S., Misra S., Pruthi P., Singhal S., Aggarwal S., Woungang I. (2011), Using bee algorithm for peer-to-peer file searching in mobile ad hoc networks, Journal of Network and Computer Applications, 34(5), 1498-1508.
- Jevtic A., Gutierrez-Martin A., Andina D.A., Jamshidi M. (2012), Distributed Bees Algorithm for Task Allocation in Swarm of Robots, IEEE Systems Journal 6(2) 296-304.

# Authors and License
The code was designed and developed by [Marco Castellani](https://www.birmingham.ac.uk/staff/profiles/mechanical/castellani-marco.aspx) (m.castellani[at]bham.ac.uk), as it can also be found on the Bees Algorithm [official site](http://beesalgorithmsite.altervista.org/).

The repository's author (Luca Baronti, **gmail** address: *lbaronti*) is responsible of the building and installation scripts, as well as making the minimal modifications of the codebase necessary for the building and use of the standalone library.

This library is released under [GPL v3 license](LICENSE).